import { $authHost, $host } from "./index";

export const createType = async (type: any) => {
  const { data } = await $authHost.post("api/type", type);
  return data;
};

export const fetchTypes = async () => {
  const { data } = await $host.get("api/type");
  return data;
};

export const createBrands = async (type: any) => {
  const { data } = await $authHost.post("api/brand", type);
  return data;
};

export const fetchBrands = async () => {
  const { data } = await $host.get("api/brand");
  return data;
};

export const createProducts = async (type: any) => {
  const { data } = await $authHost.post("api/product", type);
  return data;
};

export const fetchProducts = async () => {
  const { data } = await $host.get("api/product");
  return data;
};

export const fetchOneProduct = async (id: string) => {
  const { data } = await $host.get(`api/product/${id}`);
  return data;
};
