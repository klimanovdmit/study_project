import React, { FC } from "react";

import ProductsListPage from "../../Components/ProductsListPage";

const ProductsListPageContainer: FC = () => {
  return (
    <>
      <ProductsListPage />
    </>
  );
};

export default ProductsListPageContainer;
