import React, { FC } from "react";
import ProductPage from "../../Components/ProductPage";

const ProductPageContainer: FC = () => {
  return (
    <>
      <ProductPage />
    </>
  );
};

export default ProductPageContainer;
