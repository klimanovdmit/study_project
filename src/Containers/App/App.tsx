import React, { useEffect, useState } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { Spinner } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { authRoutes, publicRoutes } from "../../router/routes";
import { MAIN_ROUTE } from "../../utils/consts";
import { check } from "../../http/userAPI";
import { getIsAuth } from "../../store/AuthPage/selectors";
import { setIsAuthAction, setUserAction } from "../../store/AuthPage/actions";

function App() {
  const isAuth = useSelector(getIsAuth);
  // const user = useSelector(getUser);
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    check()
      .then((data) => {
        dispatch(setUserAction(data));
        dispatch(setIsAuthAction(true));
      })
      .finally(() => setLoading(false));
  }, [dispatch]);

  if (loading) {
    return <Spinner animation="grow" />;
  }
  return (
    <>
      <Switch>
        {isAuth &&
          authRoutes.map(({ path, Component }) => (
            <Route key={path} path={path} component={Component} exact />
          ))}
        {publicRoutes.map(({ path, Component }) => (
          <Route key={path} path={path} component={Component} exact />
        ))}

        <Redirect to={MAIN_ROUTE} />
      </Switch>
    </>
  );
}

export default App;
