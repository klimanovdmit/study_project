import React, { FC } from "react";
import AdminPage from "../../Components/AdminPage";

const AdminPageContainer: FC = () => {
  return (
    <>
      <AdminPage />
    </>
  );
};

export default AdminPageContainer;
