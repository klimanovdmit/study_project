import React from "react";
import style from "./BasketPage.module.scss";

class Basket extends React.Component {
  constructor(props: any) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <>
        <div className={style.page__basket}>
          <div className={style.title__card}>
            <h1>МОЯ КОРЗИНА</h1>
            <div>Товары будут зарезервированы на 60 минут</div>
          </div>
          <div className={style.position__card}>
            <h1>1</h1>
            <h2>2</h2>
          </div>
        </div>
      </>
    );
  }
}

export default Basket;
