import React from "react";
import style from "./Brands.module.scss";
import img1 from "../../../assets/image/Common/Footer/brand1.webp";
import img2 from "../../../assets/image/Common/Footer/brand2.webp";
import img3 from "../../../assets/image/Common/Footer/brand3.webp";
import img4 from "../../../assets/image/Common/Footer/brand4.webp";
import img5 from "../../../assets/image/Common/Footer/brand5.webp";
import img6 from "../../../assets/image/Common/Footer/brand6.webp";

const Brands = () => (
  <>
    <div className={style.brands}>
      <a href="/#">
        <img src={img1} alt={img1} />
      </a>
      <a href="/#">
        <img src={img2} alt={img2} />
      </a>
      <a href="/#">
        <img src={img3} alt={img3} />
      </a>
      <a href="/#">
        <img src={img4} alt={img4} />
      </a>
      <a href="/#">
        <img src={img5} alt={img5} />
      </a>
      <a href="/#">
        <img src={img6} alt={img6} />
      </a>
    </div>
  </>
);

export default Brands;
