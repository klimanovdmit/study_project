import React from "react";
import style from "./ManContant.module.scss";
import img1 from "../../../assets/image/ManContant/1.webp";
import img2 from "../../../assets/image/ManContant/2.webp";
import img3 from "../../../assets/image/ManContant/3.webp";
import img4 from "../../../assets/image/ManContant/4.webp";
import img5 from "../../../assets/image/ManContant/5.jpg";

const ManContant = () => (
  <>
    <div className={style.sale}>
      <a href="/#">
        <img src={img5} alt={img5} />
      </a>
    </div>
    <div className="container">
      <div className={style.man_contant}>
        <a href="/#">
          <img src={img1} alt={img1} />
        </a>
        <a href="/#">
          <img src={img2} alt={img2} />
        </a>
        <a href="/#">
          <img src={img3} alt={img3} />
        </a>
        <a href="/#">
          <img src={img4} alt={img4} />
        </a>
      </div>
    </div>
  </>
);

export default ManContant;
