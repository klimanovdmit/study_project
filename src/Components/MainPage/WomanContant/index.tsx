import React from "react";
import style from "./WomanContant.module.scss";
import img1 from "../../../assets/image/WomanContant/1.webp";
import img2 from "../../../assets/image/WomanContant/2.webp";
import img3 from "../../../assets/image/WomanContant/3.webp";
import img4 from "../../../assets/image/WomanContant/4.webp";
import img5 from "../../../assets/image/WomanContant/5.png";

const WomanContant = () => (
  <>
    <div className={style.sale}>
      <a href="/#">
        <img src={img5} alt={img5} />
      </a>
    </div>
    <div className="container">
      <div className={style.woman_contant}>
        <a href="/#">
          <img src={img1} alt={img1} />
        </a>
        <a href="/#">
          <img src={img2} alt={img2} />
        </a>
        <a href="/#">
          <img src={img3} alt={img3} />
        </a>
        <a href="/#">
          <img src={img4} alt={img4} />
        </a>
      </div>
    </div>
  </>
);

export default WomanContant;
