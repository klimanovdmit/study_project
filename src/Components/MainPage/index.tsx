import React, { FC } from "react";
import { useSelector } from "react-redux";
import { getGender } from "../../store/MainPage/selectors";
import PageWrapper from "../Common/Wrappers/PageWrapper";
import Brands from "./Brands";
import ManContant from "./ManContent";
import WomanContant from "./WomanContant";

const MainPage: FC = () => {
  const gender = useSelector(getGender);
  return (
    <>
      <PageWrapper>
        {gender === "man" ? <ManContant /> : <WomanContant />}
        <Brands />
      </PageWrapper>
    </>
  );
};

export default MainPage;
