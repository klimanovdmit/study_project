import React from "react";
import style from "./Button.module.scss";

type TProps = {
  children: string;
};
type TState = {
  count: number;
};

class Button extends React.Component<TProps, TState> {
  constructor(props: TProps) {
    super(props);
    this.state = { count: 0 };
  }

  plusOne = () => {
    const { count } = this.state;
    this.setState({ count: count + 1 });
  };

  render() {
    const { children } = this.props;
    return (
      <>
        <div className={style.button}>
          <a href="/#" onClick={this.plusOne}>
            {children}
          </a>
        </div>
      </>
    );
  }
}

export default Button;
