import React, { FC } from "react";

type TProps = {
  title: string;
  src: any;
  width: string;
  height: string;
};

const Image: FC<TProps> = (props: TProps) => {
  const { title, src, width, height } = props;
  return (
    <>
      <img src={src} alt={title} width={width} height={height} />
    </>
  );
};

export default Image;
