import React, { FC } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import {
  setIsAuthAction,
  setUserAction,
} from "../../../../../store/AuthPage/actions";
import { getIsAuth } from "../../../../../store/AuthPage/selectors";
import { ADMIN_ROUTE, LOGIN_ROUTE } from "../../../../../utils/consts";
import style from "./DrowerAuth.module.scss";

type TProps = {
  isAuthOpen: boolean;
  authHandler: () => void;
};

const DrowerAuth: FC<TProps> = (props: TProps) => {
  const { isAuthOpen, authHandler } = props;
  const isAuth = useSelector(getIsAuth);
  const dispatch = useDispatch();

  const logOut = () => {
    localStorage.removeItem("token");
    dispatch(setUserAction({}));
    dispatch(setIsAuthAction(false));
  };

  return (
    <>
      <div
        onMouseLeave={authHandler}
        className={`${style.drower_auth} ${isAuthOpen ? style.active : ""}`}>
        <div className={style.auth}>
          {isAuth ? (
            <button type="button" onClick={() => logOut()}>
              Выйти
            </button>
          ) : (
            <>
              <NavLink to={LOGIN_ROUTE}>
                <button type="button">Войти</button>
              </NavLink>
              <NavLink to={LOGIN_ROUTE}>
                <button type="button">Регистрация</button>
              </NavLink>
            </>
          )}
        </div>
        <div className={style.drower_links}>
          {!isAuth ? (
            <a href="/#">
              <i className="far fa-user" />
              Личный кабинет
            </a>
          ) : (
            <NavLink to={ADMIN_ROUTE}>
              <a href="/#">
                <i className="fas fa-user-cog" />
                Панель администратора
              </a>
            </NavLink>
          )}
          <a href="/#">
            <i className="fas fa-cube" />
            Мои заказы
          </a>
          <a href="/#">
            <i className="far fa-question-circle" />
            Информация о возвратах
          </a>
          <a href="/#">
            <i className="far fa-comment-dots" />
            Предпочтительные способы связи
          </a>
        </div>
      </div>
    </>
  );
};

export default DrowerAuth;
