import React, { FC, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { setGenderAction } from "../../../../store/MainPage/actions";
import { getGender } from "../../../../store/MainPage/selectors";
import { MAIN_ROUTE } from "../../../../utils/consts";
import DrowerAuth from "./DrowerAuth";
import style from "./NavbarTop.module.scss";

const NavbarTop: FC = () => {
  const [isAuthOpen, setIsAuthOpen] = useState<boolean>(false);
  const gender = useSelector(getGender);
  const dispatch = useDispatch();

  const authHandler = () => {
    setIsAuthOpen(!isAuthOpen);
  };

  const changeGender = () => {
    dispatch(setGenderAction(gender));
  };

  return (
    <>
      <div className={style.navbar__top}>
        <div className="container">
          <div className={style.navbar__top__wrapper}>
            <div className={style.logo}>
              <Link to={MAIN_ROUTE}>
                <div className={style.logo__first}>Online</div>
                <div className={style.logo__second}>STORE</div>
              </Link>
            </div>
            <div className={style.gender}>
              <div
                className={`${style.gender__inner} ${
                  gender === "woman" ? style.active : ""
                }`}
                onClick={changeGender}
                role="presentation">
                ЖЕНСКОЕ
              </div>
              <div
                className={`${style.gender__inner} ${
                  gender === "man" ? style.active : ""
                }`}
                onClick={changeGender}
                role="presentation">
                МУЖСКОЕ
              </div>
            </div>
            <div className={style.form}>
              <form action="/" method="post">
                <input type="text" placeholder="Искать товары и бренды" />
                <i className="fas fa-search" />
              </form>
            </div>
            <div className={style.links}>
              <a className=" " href="/#" onMouseEnter={authHandler}>
                <i className="fas fa-user" />
              </a>
              <a className=" " href="/#">
                <i className="fas fa-shopping-bag" />
              </a>
              <div className={style.count}>0</div>
            </div>
            <DrowerAuth isAuthOpen={isAuthOpen} authHandler={authHandler} />
          </div>
        </div>
      </div>
    </>
  );
};

export default NavbarTop;
