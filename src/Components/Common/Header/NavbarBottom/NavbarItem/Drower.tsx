import React, { FC } from "react";
import { useDispatch } from "react-redux";
import { setSelectedTypeAction } from "../../../../../store/ProductsListPage/actions";

type TProps = {
  subNav: TS[];
};
type TS = {
  id: number;
  name: string;
};

const Drower: FC<TProps> = (props: TProps) => {
  const { subNav } = props;
  const dispatch = useDispatch();

  return (
    <>
      {subNav.map((subNavItem) => (
        <li
          onClick={() => dispatch(setSelectedTypeAction(subNavItem))}
          key={`${subNavItem.name}_${subNavItem.id}`}
          role="presentation">
          {subNavItem.name}
        </li>
      ))}
    </>
  );
};

export default Drower;
