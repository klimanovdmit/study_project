import React, { FC, useState } from "react";
import { NavLink } from "react-router-dom";
import Drower from "./Drower";
import style from "../NavbarBottom.module.scss";

type TProps = {
  key: number;
  name: string;
  subNav: TS[];
  to: string;
};

type TS = {
  id: number;
  name: string;
};

const NavbarItem: FC<TProps> = (props: TProps) => {
  const { key, name, subNav, to } = props;
  const [isMenuOpen, setIsMenuOpen] = useState<boolean>(false);
  const menuOpen = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  const menuClose = () => {
    setIsMenuOpen(!isMenuOpen);
  };
  return (
    <li
      key={key}
      onMouseEnter={menuOpen}
      onMouseLeave={menuClose}
      className={style.nav__item}>
      {name}

      {subNav ? (
        <ul className={`${style.drower} ${isMenuOpen ? style.show : null}`}>
          <li>
            <NavLink to={to}>Смотреть все</NavLink>
          </li>
          <Drower subNav={subNav} />
        </ul>
      ) : null}
    </li>
  );
};

export default NavbarItem;
