import React, { FC, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import navData from "./NavbarItem/navData";
import style from "./NavbarBottom.module.scss";
import NavbarItem from "./NavbarItem";
import {
  getBrands,
  getTypes,
} from "../../../../store/ProductsListPage/selectors";
import { fetchBrands, fetchTypes } from "../../../../http/productAPI";
import {
  setBrandsAction,
  setTypesAction,
} from "../../../../store/ProductsListPage/actions";
import { PRODUCT_ROUTE } from "../../../../utils/consts";

const NavbarBottom: FC = () => {
  const types = useSelector(getTypes);
  const brands = useSelector(getBrands);
  const dispatch = useDispatch();

  useEffect(() => {
    fetchTypes().then((data) => dispatch(setTypesAction(data)));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    fetchBrands().then((data) => dispatch(setBrandsAction(data)));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const data = [
    { id: 7, name: " Обувь", subNav: types, to: `${PRODUCT_ROUTE}` },
    { id: 8, name: "Бренды", subNav: brands, to: "#" },
  ];

  return (
    <>
      <div className={style.navbar__bottom}>
        <div className="container">
          <nav className={style.nav}>
            <ul>
              {navData.map((navItem) => (
                <NavbarItem
                  key={navItem.id}
                  name={navItem.name}
                  subNav={navItem.subNav}
                  to={navItem.to}
                />
              ))}
              {data.map((navItem) => (
                <NavbarItem
                  key={navItem.id}
                  name={navItem.name}
                  subNav={navItem.subNav}
                  to={navItem.to}
                />
              ))}
            </ul>
          </nav>
        </div>
      </div>
    </>
  );
};

export default NavbarBottom;
