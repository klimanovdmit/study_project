import React from "react";
import NavbarTop from "./NavbarTop";
import NavbarBottom from "./NavbarBottom";

import style from "./Header.module.scss";

class Header extends React.Component {
  constructor(props: any) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <>
        <header className={style.header}>
          <NavbarTop />
          <NavbarBottom />
        </header>
      </>
    );
  }
}

export default Header;
