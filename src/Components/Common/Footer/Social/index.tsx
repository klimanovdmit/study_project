import React from "react";
import style from "./Social.module.scss";
import img1 from "../../../../assets/image/Common/Footer/cards.png";

const Social = () => (
  <>
    <div className={style.border}>
      <div className="container">
        <div className={style.social}>
          <div className={style.social_links}>
            <i className={`fab fa-facebook ${style.f}`} />
            <i className={`fab fa-instagram ${style.i}`} />
            <i className={`fab fa-vk ${style.v}`} />
          </div>

          <div className={style.cards}>
            <img src={img1} alt="1" />
          </div>
        </div>
      </div>
    </div>
  </>
);

export default Social;
