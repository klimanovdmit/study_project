import React from "react";
import style from "./Info.module.scss";

const Info = () => (
  <>
    <div className={style.info}>
      <div className={style.info_item}>
        <h1>ПОМОЩЬ И ИНФОРМАЦИЯ</h1>
        <ul>
          <li>
            <a href="/#">Помощь</a>
          </li>
          <li>
            <a href="/#">Отследить заказ</a>
          </li>
          <li>
            <a href="/#">Доставка и возвраты</a>
          </li>
          <li>
            <a href="/#">10% скидка для студентов</a>
          </li>
        </ul>
      </div>

      <div className={style.info_item}>
        <h1>ПОДРОБНЕЕ ОБ ONLINE STORE</h1>
        <ul>
          <li>
            <a href="/#">О нас</a>
          </li>
          <li>
            <a href="/#">Вакансии в ONLINE STORE</a>
          </li>
          <li>
            <a href="/#">Корпоративная ответственность</a>
          </li>
          <li>
            <a href="/#">Инвесторам</a>
          </li>
        </ul>
      </div>

      <div className={style.info_item}>
        <h1>БОЛЬШЕ ОБ ONLINE STORE</h1>
        <ul>
          <li>
            <a href="/#">Венсия для мобильного и приложение ONLINE STORE</a>
          </li>
          <li>
            <a href="/#">ONLINE STORE Marketplace</a>
          </li>
          <li>
            <a href="/#">Подарочные сертификаты</a>
          </li>
          <li>
            <a href="/#">Black Friday</a>
          </li>
        </ul>
      </div>

      <div className={style.info_item}>
        <h1>ШОППИНГ В</h1>
        <ul>
          <li>
            Страна: <a href="/#">ИЗМЕНИТЬ</a>
          </li>
        </ul>
      </div>
    </div>
  </>
);

export default Info;
