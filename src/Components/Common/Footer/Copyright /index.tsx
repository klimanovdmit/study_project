import React from "react";
import style from "./Copyright.module.scss";

class Copyright extends React.Component {
  constructor(props: any) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <>
        <div className={style.wrapper}>
          <div className="container">
            <div className={style.copyright}>
              <div>&copy; 2021 Online STORE</div>
              <div className={style.links}>
                <div className={style.decoraite}>
                  <a href="/#">Конфиденциальность и cookie-файлы</a>
                </div>
                <div className={style.decoraite}>
                  <a href="/#">Правила и условия</a>
                </div>
                <div>
                  <a href="/#">Доступность</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Copyright;
