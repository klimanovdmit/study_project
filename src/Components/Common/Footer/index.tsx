import React from "react";
import Copyright from "./Copyright ";
import Info from "./Info";
import Social from "./Social";

class Footer extends React.Component {
  constructor(props: any) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <>
        <Social />
        <Info />
        <Copyright />
      </>
    );
  }
}

export default Footer;
