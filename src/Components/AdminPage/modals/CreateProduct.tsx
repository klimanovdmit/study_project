import React, { FC } from "react";
import Modal from "react-bootstrap/Modal";
import { Form, Button } from "react-bootstrap";

type TProps = {
  show: boolean;
  onHide: () => void;
};

const CreateProduct: FC<TProps> = (props: TProps) => {
  const { show, onHide } = props;
  return (
    <Modal show={show} onHide={onHide} centered>
      <Modal.Header>
        <Modal.Title id="contained-modal-title-vcenter">
          Добавить тип
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Control placeholder="Введите название типа" />
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="outline-danger" onClick={onHide}>
          Закрыть
        </Button>
        <Button variant="outline-success">Добавить</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default CreateProduct;
