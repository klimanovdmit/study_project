import React, { FC, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import ProductCard from "./ProductCard";
import PageWrapper from "../Common/Wrappers/PageWrapper";
import { fetchOneProduct } from "../../http/productAPI";

type TProduct = {
  id: number;
  name: string;
  price: number;
  img: string;
  brandId: number;
  info: [];
  rating: number;
  typeId: number;
};

const ProductPage: FC = () => {
  const [product, setProduct] = useState<TProduct | any>({});
  const { id } = useParams<{ id: string }>();
  useEffect(() => {
    fetchOneProduct(id).then((data: TProduct) => setProduct(data));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <PageWrapper>
        <ProductCard
          name={product.name}
          price={product.price}
          img={product.img}
        />
      </PageWrapper>
    </>
  );
};

export default ProductPage;
