import React, { FC } from "react";

import Button from "../../Common/Button";
import style from "./ProductCard.module.scss";

type TProps = {
  name: string;
  price: number;
  img: string;
};

const ProductCard: FC<TProps> = (props: TProps) => {
  const { name, price, img } = props;
  return (
    <>
      <div className={style.page__product}>
        <div className={style.img}>
          <img src={process.env.REACT_APP_API_URL + img} alt={name} />
        </div>
        <div className={style.wrapper}>
          <div className={style.info}>
            <h1>{name}</h1>
            <h2>{price} руб.</h2>
            <a href="/#">Бесплатная доставка и возврат (Правила и условия)</a>
            <h3>ЦВЕТ:</h3>
            <h3>РАЗМЕР:</h3>
          </div>
          <Button>ДОБАВИТЬ В КОРЗИНУ</Button>
        </div>
      </div>
    </>
  );
};

export default ProductCard;
