import React, { FC } from "react";
import { useHistory } from "react-router-dom";
import { PRODUCT_ROUTE } from "../../../../utils/consts";
import Image from "../../../Common/Image";
import style from "./CardProductListItem.module.scss";

type TProps = {
  id: number;
  name: string;
  price: number;
  img: string;
  width: string;
  height: string;
};

const CardProductListItem: FC<TProps> = (props: TProps) => {
  const { id, name, price, img, width, height } = props;
  const history = useHistory();

  return (
    <>
      <li
        className={style.card__item}
        onClick={() => history.push(`${PRODUCT_ROUTE}/${id}`)}
        role="presentation">
        <div className={style.image}>
          <Image
            src={process.env.REACT_APP_API_URL + img}
            title={name}
            width={width}
            height={height}
          />
        </div>
        <h1>{`${name}`}</h1>
        <h2>{`${price} руб.`}</h2>
      </li>
    </>
  );
};

export default CardProductListItem;
