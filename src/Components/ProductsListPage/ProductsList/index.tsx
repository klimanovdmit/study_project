import React, { FC } from "react";
import CardProductListItem from "./ProductCardListItem";
import style from "./ProductsList.module.scss";

type TProps = {
  products: Tp[];
};

type Tp = {
  id: number;
  name: string;
  price: number;
  img: string;
};

const ProductsList: FC<TProps> = (props: TProps) => {
  const { products } = props;
  return (
    <>
      <div className="container">
        <div className={style.products__list}>
          <p>Найдено {`${products.length}`} моделей</p>
          <ul>
            {products.map((product) => {
              return (
                <CardProductListItem
                  id={product.id}
                  name={product.name}
                  price={product.price}
                  img={product.img}
                  width="100%"
                  height="auto"
                />
              );
            })}
          </ul>
        </div>
      </div>
    </>
  );
};

export default ProductsList;
