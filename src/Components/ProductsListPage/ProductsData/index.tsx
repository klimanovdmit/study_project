import image1 from "../../../assets/image/products/1.jpeg";
import image2 from "../../../assets/image/products/2.jpeg";
import image3 from "../../../assets/image/products/3.jpeg";
import image4 from "../../../assets/image/products/4.jpeg";
import image5 from "../../../assets/image/products/5.jpeg";
import image6 from "../../../assets/image/products/6.jpeg";
import image7 from "../../../assets/image/products/7.jpeg";
import image8 from "../../../assets/image/products/8.jpeg";
import image9 from "../../../assets/image/products/9.jpeg";
import image10 from "../../../assets/image/products/10.jpeg";
import image11 from "../../../assets/image/products/11.jpeg";
import image12 from "../../../assets/image/products/12.jpeg";

const dataProducts = [
  {
    id: "a1",
    title: "Кроссовки Jordan Series.01",
    price: 6990,
    src: [image1],
  },
  {
    id: "a2",
    title: "Мужские кроссовки New Balance Fuelcell Speedrift",
    price: 15500,
    src: [image2],
  },
  {
    id: "a3",
    title: "Мужские кроссовки New Balance 574",
    price: 12300,
    src: [image3],
  },

  {
    id: "a4",
    title: "Мужские кроссовки New Balance 530",
    price: 9900,
    src: [image4],
  },
  {
    id: "a5",
    title: "Мужские кроссовки New Balance 2002 ",
    price: 12250,
    src: [image5],
  },
  {
    id: "a6",
    title: "Мужские кроссовки New Balance 2002",
    price: 12250,
    src: [image6],
  },
  {
    id: "a7",
    title: "Мужские кроссовки Nike SB Zoom Blazer Low Pro GT",
    price: 6790,
    src: [image7],
  },
  {
    id: "a8",
    title: "Мужские кроссовки Jordan 11 CMFT Low",
    price: 10790,
    src: [image8],
  },
  {
    id: "a9",
    title: "Мужские кроссовки Jordan Delta 2",
    price: 11290,
    src: [image9],
  },
  {
    id: "a10",
    title: "Мужские кроссовки Nike Air Presto",
    price: 10490,
    src: [image10],
  },
  {
    id: "a11",
    title: "Мужские кроссовки Nike Air Huarache",
    price: 10490,
    src: [image11],
  },
  {
    id: "a12",
    title: "Мужские кроссовки Nike Air Force 1 '07",
    price: 8990,
    src: [image12],
  },
];

export default dataProducts;
