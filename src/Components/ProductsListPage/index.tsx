import React, { FC, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchProducts } from "../../http/productAPI";
import { setProductsAction } from "../../store/ProductsListPage/actions";
import { getProducts } from "../../store/ProductsListPage/selectors";
import PageWrapper from "../Common/Wrappers/PageWrapper";
import ProductsList from "./ProductsList";

const ProductsListPage: FC = () => {
  const products = useSelector(getProducts);
  const dispatch = useDispatch();
  useEffect(() => {
    fetchProducts().then((data) => dispatch(setProductsAction(data.rows)));
  }, [dispatch]);
  return (
    <>
      <PageWrapper>
        <ProductsList products={products} />
      </PageWrapper>
    </>
  );
};

export default ProductsListPage;
