import React, { FC, useState } from "react";
import { useDispatch } from "react-redux";

import { NavLink, useHistory, useLocation } from "react-router-dom";
import { login, registration } from "../../http/userAPI";
import { setIsAuthAction, setUserAction } from "../../store/AuthPage/actions";
import {
  LOGIN_ROUTE,
  MAIN_ROUTE,
  REGISTRATION_ROUTE,
} from "../../utils/consts";

import style from "./Auth.module.scss";

const AuthPage: FC = () => {
  const location = useLocation();
  const history = useHistory();
  const isLogin = location.pathname === LOGIN_ROUTE;
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();

  const click = async () => {
    try {
      let data;
      if (isLogin) {
        data = await login(email, password);
      } else {
        data = await registration(email, password);
      }
      dispatch(setUserAction(data));
      dispatch(setIsAuthAction(true));
      history.push(MAIN_ROUTE);
    } catch (e) {
      // eslint-disable-next-line no-alert
      alert(e.response.data.message);
    }
  };

  return (
    <>
      <div className={style.auth_page}>
        <NavLink to={MAIN_ROUTE}>
          <div className={style.logo}>
            <div className={style.logo__second}>
              STORE
              <div className={style.logo__first}>Online</div>
            </div>
          </div>
        </NavLink>
        <div className="container">
          <div className={style.auth_block}>
            <div className={`${style.auth} ${style.active}`}>
              <NavLink to={REGISTRATION_ROUTE}>
                <button type="button">РЕГИСТРАЦИЯ</button>
              </NavLink>

              <NavLink to={LOGIN_ROUTE}>
                <button type="button">ВОЙТИ</button>
              </NavLink>
            </div>
            <div className={style.form}>
              <div>АДРЕС ЭЛЕКТРОННОЙ ПОЧТЫ:</div>
              <input
                placeholder="Введите Ваш email..."
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                type="textarea"
              />
              <div>ПАРОЛЬ:</div>
              <input
                placeholder="Введите Ваш пароль..."
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                type="password"
              />
              <button onClick={click} type="submit">
                {isLogin ? "ВОЙТИ" : "ЗАРЕГИСТРИРОВАТЬСЯ"}
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AuthPage;
