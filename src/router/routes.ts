import {
  MAIN_ROUTE,
  ADMIN_ROUTE,
  BASKET_ROUTE,
  LOGIN_ROUTE,
  REGISTRATION_ROUTE,
  PRODUCT_ROUTE,
} from "../utils/consts";
import AdminPageContainer from "../Containers/AdminPageContainer";
import BasketPageContainer from "../Containers/BasketPageContainer";
import MainPageContainer from "../Containers/MainPageContainer";
import AuthPageContainer from "../Containers/AuthPageContainer";
import ProductsListPageContainer from "../Containers/ProductsListPageContainer";
import ProductPageContainer from "../Containers/ProductPageContainer";

export const authRoutes = [
  {
    path: ADMIN_ROUTE,
    Component: AdminPageContainer,
  },
  {
    path: BASKET_ROUTE,
    Component: BasketPageContainer,
  },
];

export const publicRoutes = [
  {
    path: MAIN_ROUTE,
    Component: MainPageContainer,
  },
  {
    path: LOGIN_ROUTE,
    Component: AuthPageContainer,
  },
  {
    path: REGISTRATION_ROUTE,
    Component: AuthPageContainer,
  },
  {
    path: PRODUCT_ROUTE,
    Component: ProductsListPageContainer,
  },
  {
    path: `${PRODUCT_ROUTE}/:id`,
    Component: ProductPageContainer,
  },
];
