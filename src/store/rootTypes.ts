import { TProductsListReducer } from "./ProductsListPage/types";
import { TMainReducer } from "./MainPage/types";
import { TAuthReducer } from "./AuthPage/types";

export type TState = {
  authPage: TAuthReducer;
  mainPage: TMainReducer;
  productsListPage: TProductsListReducer;
};
