export type TProductsListReducer = {
  types: Tt[];
  brands: Tb[];
  products: Tp[];
  selectedType: any;
};

export type Tt = {
  id: number;
  name: string;
};

export type Tb = {
  id: number;
  name: string;
};

export type Tp = {
  id: number;
  name: string;
  price: number;
  rating: number;
  img: string;
};
