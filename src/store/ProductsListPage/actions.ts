import { Tb, Tp, Tt } from "./types";

export enum Actions {
  setTypes = "SET_TYPES",
  setBrands = "SET_BRANDS",
  setProducts = "SET_PRODUCTS",
  setSelectedType = "SET_SELECTED_TYPE",
}

export const setTypesAction = (types: Tt[]) => {
  return {
    type: Actions.setTypes,
    payload: types,
  };
};

export const setBrandsAction = (brands: Tb[]) => {
  return {
    type: Actions.setBrands,
    payload: brands,
  };
};

export const setProductsAction = (products: Tp[]) => {
  return {
    type: Actions.setProducts,
    payload: products,
  };
};

export const setSelectedTypeAction = (type: any) => {
  return {
    type: Actions.setSelectedType,
    payload: type,
  };
};
