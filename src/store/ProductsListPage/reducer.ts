import { Actions } from "./actions";
import { TProductsListReducer } from "./types";

const initialState: TProductsListReducer = {
  types: [],

  brands: [],

  products: [],

  selectedType: {},
};

const productsListReducer = (
  state = initialState,
  action: { type: string; payload?: any }
) => {
  switch (action.type) {
    case Actions.setTypes:
      return { ...state, types: action.payload };
    case Actions.setBrands:
      return { ...state, brands: action.payload };
    case Actions.setProducts:
      return { ...state, products: action.payload };
    // case Actions.setSelectedType:
    //   return { ...state, selectedType: action.payload };
    default:
      return state;
  }
};

export default productsListReducer;
