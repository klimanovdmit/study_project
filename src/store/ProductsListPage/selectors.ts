import { TState } from "../rootTypes";

// eslint-disable-next-line import/prefer-default-export
export const getTypes = (state: TState) => state.productsListPage.types;
export const getBrands = (state: TState) => state.productsListPage.brands;
export const getProducts = (state: TState) => state.productsListPage.products;
export const getSelectedType = (state: TState) =>
  state.productsListPage.selectedType;
