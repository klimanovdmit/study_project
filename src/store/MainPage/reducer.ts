import { Actions } from "./actions";
import { TMainReducer } from "./types";

const initialState: TMainReducer = {
  gender: "man",
};

const mainReducer = (
  state = initialState,
  action: { type: string; payload?: any }
) => {
  switch (action.type) {
    case Actions.setGender:
      return { ...state, gender: action.payload };
    default:
      return state;
  }
};

export default mainReducer;
