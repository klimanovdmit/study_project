import { TState } from "../rootTypes";

// eslint-disable-next-line import/prefer-default-export
export const getGender = (state: TState) => state.mainPage.gender;
