export enum Actions {
  setGender = "SET_GENDER",
}

export const setGenderAction = (gender: string) => {
  return {
    type: Actions.setGender,
    payload: gender === "man" ? "woman" : "man",
  };
};
