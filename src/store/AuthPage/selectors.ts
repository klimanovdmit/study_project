import { TState } from "../rootTypes";

// eslint-disable-next-line import/prefer-default-export
export const getIsAuth = (state: TState) => state.authPage.isAuth;

export const getUser = (state: TState) => state.authPage.user;
