export enum Actions {
  setIsAuth = "SET_IS_AUTH",
  setUser = "SET_USER",
}

export const setIsAuthAction = (bool: boolean) => {
  return {
    type: Actions.setIsAuth,
    payload: bool,
  };
};

export const setUserAction = (user: any) => {
  return {
    type: Actions.setUser,
    payload: user,
  };
};
