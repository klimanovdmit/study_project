import { Actions } from "./actions";
import { TAuthReducer } from "./types";

const initialState: TAuthReducer = {
  isAuth: false,
  user: {},
};

const authReducer = (
  state = initialState,
  action: { type: string; payload?: any }
) => {
  switch (action.type) {
    case Actions.setIsAuth:
      return { ...state, isAuth: action.payload };
    case Actions.setUser:
      return { ...state, user: action.payload };
    default:
      return state;
  }
};

export default authReducer;
