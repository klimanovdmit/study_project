import { combineReducers } from "redux";
import authReducer from "./AuthPage/reducer";
import mainReducer from "./MainPage/reducer";
import productsListReducer from "./ProductsListPage/reducer";

const rootReducer = combineReducers({
  authPage: authReducer,
  mainPage: mainReducer,
  productsListPage: productsListReducer,
});

export default rootReducer;
